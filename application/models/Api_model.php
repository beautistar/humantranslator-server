<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }     
    
    function email_exist($email) {
        
        $this->db->where('email', $email);
        if ($this->db->get('tb_user')->num_rows() > 0) {
            
            return 1;
        } else {
            return 0;
        }
    }
    
    function getUserIdByEmail($email) {
        
        return $this->db->where('email', $email)->get('tb_user')->row()->id;
    }

    
    function register($name, $email, $password_hash, $user_type) {
        
        $this->db->set('name', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password_hash);
        $this->db->set('user_type', $user_type);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        $user_id = $this->db->insert_id();
        
        return $user_id;
    }
    
    function uploadPhoto($user_id, $file_url) {
        
        $this->db->where('id', $user_id);
        $this->db->set('photo_url', $file_url);
        $this->db->update('tb_user');
        
    }
    
    function updateProfile($user_id, $gender, $age, $my_language, $trans_language, $about_me, $is_available) {
        
        $this->db->where('id', $user_id);
        $this->db->set('gender', $gender);
        $this->db->set('age', $age);
        $this->db->set('my_language', $my_language);
        $this->db->set('trans_language', $trans_language);
        $this->db->set('about_me', $about_me);
        $this->db->set('is_available', $is_available);
        $this->db->update('tb_user');
    }
    
    function resetPassword($email, $password_hash) {
        
        $this->db->where('email', $email);
        $this->db->set('password', $password_hash);
        $this->db->update('tb_user');
    }
    
    function setAvailable($user_id, $is_available) {
        
        $this->db->where('id', $user_id);
        $this->db->set('is_available', $is_available);
        $this->db->update('tb_user');
    }
    
    function writeReview($sender_id, $target_id, $mark, $review) {
        
        
        //$this->db->where('sender_id', $sender_id);
        //$this->db->where('target_id', $target_id);

        //if ($this->db->get('tb_review')->num_rows() == 0) {
            
        $this->db->set('sender_id', $sender_id);
        $this->db->set('target_id', $target_id);
        $this->db->set('mark', $mark);
        $this->db->set('review', $review);
        $this->db->insert('tb_review');
        $this->db->insert_id();
        
        // set target user evelate mark.
        
        $this->db->where('target_id', $target_id);
        $query = $this->db->get('tb_review');
        
        $count = $query->num_rows();
        
        $total_mark = 0;
        foreach ($query->result() as $review) {
            
            $total_mark += $review->mark;
        }    
        
        $avg_mark = $total_mark / $count;
        
        $this->db->where('id', $target_id);
        $this->db->set('avg_mark', $avg_mark);
        $this->db->update('tb_user');
        //} 
    }
    
    function searchTranslator($my_language, $trans_language) {
        
        $result = array();
        
        //$this->db->where('user_type', 1);
        //$this->db->where('my_language', $my_language);
        //$this->db->where('trans_language', $trans_language);
        
        $sql = "SELECT * from tb_user WHERE user_type = 1 AND (my_language = '$my_language' AND trans_language = '$trans_language') OR (my_language = '$trans_language' AND trans_language = '$my_language')";
        
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            foreach($query->result() as $translator) {
                
                $arr = array('user_id' => $translator->id, 
                             'name' => $translator->name, 
                             'gender' => $translator->gender,
                             'age' => $translator->age,
                             'about_me' => $translator->about_me,
                             'is_available' => $translator->is_available,
                             'photo_url' => $translator->photo_url,
                             'avg_mark' => $translator->avg_mark);
                             
                array_push($result, $arr);
                
            }
        }
        
        return $result;
    }
    
    function getReview($user_id) {
        
        $result = array();
        
        $this->db->select('*');
        $this->db->from('tb_review');
        $this->db->where('tb_review.target_id', $user_id);
        $this->db->join('tb_user', 'tb_review.sender_id = tb_user.id');        
        //$this->db->group_by('tb_user.id');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $review) {
                
                $arr = array('user_id' => $review->sender_id,
                             'name' => $review->name,
                             'photo_url' => $review->photo_url,
                             'mark' => $review->mark,
                             'review' => $review->review);
                array_push($result, $arr);
            }
        }
        
        return $result;
        
    }
    
    function saveToken($user_id, $token) {
        
        $this->db->where('id', $user_id);
        $this->db->set('token', $token);
        $this->db->update('tb_user');
    }
    
    function getToken($user_id) {
        
        return $this->db->where('id', $user_id)->get('tb_user')->row()->token;
        
    }
    
    function getPhotoUrl($user_id) {
        
        return $this->db->where('id', $user_id)->get('tb_user')->row()->photo_url;
        
    }
    
    function sendRequest($user_id, $target_id, $room_no) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('target_id', $target_id);
        
        if ($this->db->get('tb_call')->num_rows() == 0) {
            $this->db->set('user_id', $user_id);
            $this->db->set('target_id', $user_id);
            $this->db->set('room_no', $room_no);
            $this->db->set('status', 0);
            $this->db->set('requested_date', 'NOW()', false);
            $this->db->insert('tb_call');
            $this->db->insert_id();
        } else {
            $this->db->where('user_id', $user_id);
            $this->db->where('target_id', $target_id);
            $this->db->set('room_no', $room_no);
            $this->db->set('status', 0);
            $this->db->set('requested_date', 'NOW()', false);
            $this->db->update('tb_call');
        }
    }
    
    function acceptRequest($user_id, $target_id, $room_no) {
        
        $this->db->where('user_id', $user_id);
        $this->db->where('target_id', $target_id);        
        $this->db->set('status', 1);         
        $this->db->update('tb_call');
        
    } 
}
?>
