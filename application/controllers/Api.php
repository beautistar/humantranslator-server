<?php


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    function version() {
        phpinfo();
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

         $this->doRespond(100, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
     
    function sendPush($target_id, $type, $body, $content, $user_id, $photo_url) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAA34MKInQ:APA91bEx3lMJnfg2pySIaenecadjgvOcKLVUH97gkdD0DKs78LGSSpCznHeE8L3ailUzCBWPWFANw0Oejr3se7O0biNm1Hud7NVKi1TraP3ZYsNVIToNjsg9HatuN9O6cPSUtlBQlcgT";
        
        $token = "";
        $token = $this->api_model->getToken($target_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
//       if(is_array($target)){
//        $fields['registration_ids'] = $target;
//       }else{
//        $fields['to'] = $target;
//           }

        // Tpoic parameter usage
//        $fields = array
//                    (
//                        'to'  => '/topics/alerts',
//                        'notification'          => $msg
//                    );
        $data = array('msg_type' => $type,
                      'room_no' => $content,
                      'user_id' => $user_id, 
                      'photo_url' => $photo_url);
        
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Human Translator',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    } 
    
    function sendPushAll($tokens, $group_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAA_q6-rEA:APA91bFMVI95McE26k71yJV3fzgriMXjVa-fgUdfyozR_7G6-a3b7oWTpzpualrJINg1VWyXb32Rxe0PXg1vKG0XK7T3AhhXf0YvFb07ivJciC4psDq4JWOmW53WX-cJr0f4zsYx0sTc";
        
        //        $tokens = array();
        //        foreach ($user_ids as $user_id) {
        //            array_push($tokens, $this->api_model->getToken($user_id));
        //        }
        if (count($tokens) == 0 ) {

            return;
        }

        $data = array('msg_type' => $type,
                      'groupId' => $group_id,  
                      'content' => json_encode($content));
        
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Human Translator',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
        $fields = array
            (
                'registration_ids'  => $tokens,
                //'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        //@curl_exec($ch);
        
        $p_result  = curl_exec($ch);
        
        if ($p_result === false) {
            $result['error'] = curl_error($ch);
        }
        $result['result'] = $p_result;        
        
        curl_close($ch); 
        
        return $result;
        
    } 
     
    function signup() {
         
         $result = array();         
         $name = $_POST['name'];
         $email = $_POST['email'];
         $password = $_POST['password'];
         $user_type = $_POST['user_type'];
         
         $email_exist = $this->api_model->email_exist($email);
         
         if ($email_exist > 0) {
             $this->doRespond(101, $result);
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($password, PASSWORD_BCRYPT);
         
         $user_id = $this->api_model->register($name, $email, $password_hash, $user_type);
         $result['user_id'] = $user_id;
         
         $this->doRespondSuccess($result);
    }
    
    function uploadPhoto() {
        
        $result = array();
         
        $user_id = $_POST['user_id'];
        
        if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $user_id.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $id = $this->api_model->uploadPhoto($user_id, $file_url);
            $result['photo_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(102, $result);// upload fail
            return;
        }
        
    }
    
    function updateProfile() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $gender = $_POST['gender'];
         $age = $_POST['age'];
         $my_language = $_POST['my_language'];
         $trans_language = $_POST['trans_language'];
         $about_me = $_POST['about_me'];
         $is_available = $_POST['is_available'];                  
         
         $this->api_model->updateProfile($user_id, $gender, $age, $my_language, $trans_language, $about_me, $is_available);
         $this->doRespondSuccess($result);             
    }
     
    function login() {
         
         $result = array();  
         $email = $_POST['email'];
         $password = $_POST['password'];
         
         $email_exist = $this->api_model->email_exist($email);
         
         if ($email_exist == 0) {             
             $this->doRespond(103, $result);
             return;
         }
         
         if(!function_exists('password_verify'))
             $this->load->helper('password'); // password_helper.php loading

         $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
         $pwd = $row->password;

         if (!password_verify($password, $pwd)){  // wrong password.
             
             $this->doRespond(104, $result);
             return;

         } else {
             
             $result['userModel'] = array('id' => $row->id,
                                          'user_type' => $row->user_type,
                                          'name' => $row->name,
                                          'email' => $email,
                                          'password' => $password,
                                          'age' => $row->age,
                                          'gender' => $row->gender,
                                          'about_me' => $row->about_me,
                                          'my_language' => $row->my_language,
                                          'trans_language' => $row->trans_language,
                                          'is_available' => $row->is_available,
                                          'photo_url' =>$row->photo_url,
                                          'avg_mark' => $row->avg_mark
                                          ); 
         }

         $this->doRespondSuccess($result);  
         
    }
    
    function resetPassword() {
        
        $result = array();         
        $email = $_POST['email'];
        $password = $_POST['password'];
         
        $email_exist = $this->api_model->email_exist($email);
         
        if ($email_exist == 0) {
            $this->doRespond(103, $result);
            return;
        }
         
        if(!function_exists('password_hash'))
             $this->load->helper('password');

        $password_hash = password_hash($password, PASSWORD_BCRYPT);
         
        $user_id = $this->api_model->resetPassword($email, $password_hash);
         
        $this->doRespondSuccess($result);
        
    }
    
    function setAvailable() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $is_available = $_POST['is_available'];
        
        $this->api_model->setAvailable($user_id, $is_available);
        
        $this->doRespondSuccess($result);
    }
    
    function writeReview() {
        
        $result = array();
        $sender_id = $_POST['user_id'];
        $target_id = $_POST['target_id'];
        $mark = $_POST['mark'];
        $review = $_POST['review'];
        
        $this->api_model->writeReview($sender_id, $target_id, $mark, $review);
        
        $this->doRespondSuccess($result);
    }
    
    function searchTranslator() {
        
        $result = array();
        $my_language = $_POST['my_language'];
        $trans_language = $_POST['trans_language'];
        
        $result['translators'] = $this->api_model->searchTranslator($my_language, $trans_language);
        
        $this->doRespondSuccess($result);
    }
    
    function getReview() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        
        $result['reviews'] = $this->api_model->getReview($user_id);
        
        $this->doRespondSuccess($result);
    }
    
    function saveToken() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $token = $_POST['token'];
         
         $this->api_model->saveToken($user_id, $token);
         
         $this->doRespondSuccess($result); 
    }
    
    function sendRequest() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $name = $_POST['name'];
        $target_id = $_POST['target_id'];
        $room_no = $_POST['room_no'];
        
        $photo_url = $this->api_model->getPhotoUrl($user_id);        
        
        $this->api_model->sendRequest($user_id, $target_id, $room_no);
        $result['push'] = $this->sendPush($target_id, "request", $name." sent you request", $room_no, $user_id, $photo_url);
        
        $this->doRespondSuccess($result);
    }
    
    function acceptRequest() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $name = $_POST['name'];
        $target_id = $_POST['target_id'];
        $room_no = $_POST['room_no'];
        
        $this->api_model->acceptRequest($user_id, $target_id, $room_no);
        $this->sendPush($user_id, "accept", $name." accept your request", $room_no, 0, "");
        $this->doRespondSuccess($room_no);        
        
    }
    
    function rejectRequest() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $name = $_POST['name'];
        $target_id = $_POST['target_id'];
        $room_no = $_POST['room_no'];
        
        $photo_url = $this->api_model->getPhotoUrl($user_id);        
        
        //$this->api_model->sendRequest($user_id, $target_id, $room_no);
        $result['push'] = $this->sendPush($target_id, "reject", $name." reject your request", $room_no, $user_id, $photo_url);
        
        $this->doRespondSuccess($result);
        
    }
                         
}

?>
